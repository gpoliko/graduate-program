# WELCOME TO ENHANCE!
### This is your training hub :rocket:
---
__Graduate Dashboard__

This is where you will be working through the Enhance graduate technical training. To get started, click on the link to the Sprint you are currently on and work through the readings, primers, and challenges for that sprint. We recommend these be completed in the order they are listed as they will contain information necessary to complete the next part.

## Sprints
[Sprint 1](/sprints/sprint-1/README.md) - Start here!

[Sprint 2](#)

[Sprint 3](#) 

---
Resources (link)

Feedback (link)