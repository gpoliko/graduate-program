[Sprint 1 Home](README.md)  | 
------------|

# Command Line Primer (aka Terminal, Shell, Prompt)

### Learning Competencies
By the end of this exploration, you should be able to do the following in/using the command line:
- Identify your location
- Navigate between directories
- Create new and delete existing directories and files
- Open files

## Introduction 
The purpose of this primer is to introduce you to the command line tool. If you're already familiar with it, check your knowledge against the learning competencies. If you are, have a crack at the stretch competencies. 

Programmers do not always have pretty ways to interface or test their code. Most spend a good deal of time in their command line - "a tool where you can use text commands to perform specific tasks." When you use the command line, you don't slow yourself down opening a finder and double-clicking on folders. Instead, you get to navigate around your computer, open, create, delete, move, files, etc. all using commands. It's quick, efficient, and easy once you get the hang of it.

Knowing how to use your command line will be vital to your success as a engineer. Fortunately for now you only need to know a few commands.

## See it in action 
[Mac and Linux - _basic terminal Usage, youtube_](https://www.youtube.com/watch?v=jDINUSK7rXE&list=PLoYCgNOIyGAB_8_iq1cL8MVeun7cB6eNc&index=16)

## Give it a try
Follow this [useful guide](https://learnpythonthehardway.org/book/appendix-a-cli/ex1.html) to try it out. Don't be put off by the "learn python" reference. Notice the difference between windows, Linux and mac.

## Explore a little more 
Have a look for some learning resources: Videos, tutorials, Google. Try key words like 'Terminal', 'Command Line Basic' and 'cheatsheet'.

## Stretch Competencies
Come back to stretch competencies in your spare time. Stretch material is useful, but isn't necessary to move forward. Don't focus on them now while you're getting started, but come back to them when you have spare time.

- Remove or copy a directory  
- Stream a file  
- Find and look inside files  
- Get help  

- [Codecademy's command line course](https://www.codecademy.com/courses/learn-the-command-line/l)  
- [Udemy's Command Line Basics course](https://www.udacity.com/course/linux-command-line-basics--ud595)  
- [Udacity's Shell Workshop](https://classroom.udacity.com/courses/ud206/lessons/1cc90ac3-c03a-43e6-8556-dcbd40dfd418/concepts/9fbb976b-dc7b-4584-a656-b05ab9d7a0c3)   
