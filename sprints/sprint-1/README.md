[Dashboard](/README.md)|
---|

# Sprint One

__Device Setup, CommandLine/Terminal, and Bitbucket__

In this sprint you will setup your device and Enhance your understandings of Git, Bitbucket and the Terminal (the tools and workflow that you will use throughout your Enhance magic creation).
 

## Enhancing
1. [Account creation](1-account-creation.md)
2. [Device "First time" Setup](1-mac-setup.md)
3. [Java Programming Language - Primer](1-java-primer.md)
4. [Command Line - Primer](1-commandline-primer.md)
5. [Version Control with Git - Primer](#)
6. [Install and Explore Git and Bitbucket - Course](#)
7. [Branch, Fetch/Pull, Rebase/Merge - Primer](#)
8. [Branch, Fetch/Pull, Rebase/Merge - Challenge](#)
9. [BitBucket Fork and Clone - Challenge](#)


## Stretch
[Bitbucket stretch](#)

## Before you go
1. [Feedback](#)
2. Make sure all your necessary work has been pushed up to Bitbucket.
