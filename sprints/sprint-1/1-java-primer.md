[Sprint 1 Home](README.md)  | 
------------|

# Java Programming Primer

### Learning Competencies
By the end of this module, you should have learned the basics of programming in the Java programming langauge, or atleast given yourself a refresher on these concepts.


## Summary
The Enhance Automation Framework is written entirely in Java, so it's best to be familar and competent with the Java programming language.


## Let's get into it!
Work through this [content](https://www.codecademy.com/learn/learn-java). You can ignore the concepts related to *"Access, Encapsulation, and Static Methods"* and *"Inheritence and Polymorphism"*. But feel free to work through them if it interests you.