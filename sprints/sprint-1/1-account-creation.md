# Accounts To Create

For all accounts that you create please use your prefered first and last names. Not just your first name, and don't use creative nicknames. For example:

**Bob Smith** is fine.

**Robbie Smith** is fine.

**Robert Smith** is fine.

**Robert** or **Robert S.** is not fine (add your full last name).

**xxxKawaiRobbiexxx** is not fine (use your actual name, clients often see these!)​​
## Accounts

Create a Bitbucket account with your work email: [Bitbucket](www.bitbucket.org)

Create a Slack account with your work email: [Slack](www.slack.com)

If setting up a Mac for iOS automation, create a new Apple user ID using your work email. You do not need to do this if you are not doing IOS automation as Macs will allow you to use them without an apple ID.

Enhance webmail link: https://login.microsoftonline.com/ and use your Enhance credentials. 


Make sure you have been given your details for TimeFiler (talk to B/Larissa if you have not received these) [TimeFiler](https://enhance-employees.imsonline.co.nz/​)

​